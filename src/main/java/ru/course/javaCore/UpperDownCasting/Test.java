package ru.course.javaCore.UpperDownCasting;

public class Test {
    public static void main(String[] args) {
        Dog dog = new Dog();
        //Upcasting - восходное преобразование
        Animal animal = dog;

        //Downcasting - Нисходящее преобразование
        Dog dog2 = (Dog) animal;

    }
}
