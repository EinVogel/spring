package ru.course.javaCore.accessModifiers.nameSpace2;

import ru.course.javaCore.accessModifiers.nameSpace1.Person;

public class Worker extends Person {

    public static void main(String[] args) {
        Worker worker = new Worker();
        System.out.println(worker.age);
        System.out.println("Has person any soul?: " + worker.SOUL);
    }
}
