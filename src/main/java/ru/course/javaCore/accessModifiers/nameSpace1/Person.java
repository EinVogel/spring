package ru.course.javaCore.accessModifiers.nameSpace1;

public class Person {
    // Модификатор доступа default. Виден только в пакете nameSpace1
    String name = "Bob";
    // Модификатор доступа public. Виден со всех пакетов, но следует упостреблять только с константами
    public final Boolean SOUL = true;
    // Модификатор доступа protected. Виден в пакете nameSpace1 и в других, если из класса, который содержит protected
    // наследоваться в класс из nameSpace2
    protected String age;
    // Модификатор доступа private. Виден только в классе Person
    private int phone_number;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public int getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(int phone_number) {
        this.phone_number = phone_number;
    }

}
