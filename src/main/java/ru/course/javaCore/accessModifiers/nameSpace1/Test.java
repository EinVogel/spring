package ru.course.javaCore.accessModifiers.nameSpace1;

public class Test {
    public static void main(String[] args){
        Person person = new Person();
        System.out.println("Name: " + person.name);
        System.out.println("Age: " + person.age);
    }
}
