package ru.course.javaCore;

import java.util.Scanner;

public class Loops {
    public static void main(String[] args){
        int a = 5;
        while(a > 0){
            System.out.println(a);
            a -= 1;
        }

        for(int i = 0; i<10; i++){
            System.out.println(i);
        }

        // Ввод значений с консоли
        Scanner scanner = new Scanner(System.in);
        int value;
        do{
            System.out.println("Введите 5");
            value = scanner.nextInt();
        }while (value!=5);

    }
}
