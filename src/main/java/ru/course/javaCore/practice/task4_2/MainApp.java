package ru.course.javaCore.practice.task4_2;

import java.util.ArrayList;

public class MainApp {
    public static void main(String[] args) {
        int[] array = {1, 2, 3};
        ArrayList<Integer> arrayList = transformType(array);
        System.out.println(arrayList);
    }

    static ArrayList<Integer> transformType(int... array){
        ArrayList<Integer> arrayList = new ArrayList<>();
        for (int item: array){
            arrayList.add(item);
        }
        return arrayList;
    }
}
