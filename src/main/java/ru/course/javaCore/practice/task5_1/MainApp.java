package ru.course.javaCore.practice.task5_1;

import java.util.*;

public class MainApp {
    public static void main(String[] args) {
        List<String> wordsList = new ArrayList<>(Arrays.asList(
                "Дом",
                "Дом",
                "Дерево",
                "Стол",
                "Поляна",
                "Небо",
                "Стакан",
                "Небо",
                "Стол",
                "Палитра",
                "Кипяток"
        ));
        Set<String> set = new HashSet<>(wordsList);
        System.out.println(set);
    }
}
