package ru.course.javaCore.practice.task4_1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainApp {
    public static void main(String[] args) {
        ArrayList<String> arrayList1 = new ArrayList<>(Arrays.asList("A", "B", "C"));
        ArrayList<String> arrayList2 = new ArrayList<>(Arrays.asList("F", "D", "M"));
        changeItems(arrayList1, arrayList2, 2);

    }

    static void changeItems(List<String> array1, List<String> array2, int indexToChange){
       String item1 = array1.get(indexToChange);
       String item2 = array2.get(indexToChange);
       if (array1.size() == array2.size()) {
           array1.set(indexToChange, item2);
           array2.set(indexToChange, item1);
       }
        System.out.println("Массив array1: " + array1);
        System.out.println("Массив array2: " + array2);
    }
}
