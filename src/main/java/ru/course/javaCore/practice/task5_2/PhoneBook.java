package ru.course.javaCore.practice.task5_2;

import java.util.*;

public class PhoneBook {

    Map<String, Set<String>> book = new HashMap<>();

    void addUserPhone(String lastName, String number){
        if (book.get(lastName) == null){
            Set<String> userPhones = new HashSet<>(Arrays.asList(number));
            book.put(lastName, userPhones);
        }else {
            Set<String> userPhones = book.get(lastName);
            userPhones.add(number);
        }
    }

    void getUserPhone(String lastName){
        Set<String> userPhones = book.get(lastName);
        System.out.println(userPhones);
    }
}
