package ru.course.javaCore.practice.task5_2;

public class MainApp {
    public static void main(String[] args) {
        PhoneBook phoneBook = new PhoneBook();
        phoneBook.addUserPhone("Иванов", "89612764211");
        phoneBook.addUserPhone("Иванов", "89612764221");
        phoneBook.getUserPhone("Иванов");
    }
}
