package ru.course.javaCore.practice.task1;

public abstract class Animal {
    String type;
    String name;
    int maxRunDistance;
    int maxSwimDistance;
    public static int count;

    public Animal(String type, String name, int maxRunDistance, int maxSwimDistance) {
        this.type = type;
        this.name = name;
        this.maxRunDistance = maxRunDistance;
        this.maxSwimDistance = maxSwimDistance;
        count++;
    }

    void run(int distance){
        if (distance <= maxRunDistance){
            System.out.println(type + " " + name + " справляется с забегом");
        }else{
            System.out.println(type + " " + name + " не справляется с забегом");
        }
    }


    void swim(int distance) {
        if (maxSwimDistance == 0) {
            System.out.println(type + " " + name + " не умеет плавать");
            return;
        }

        if (distance <= maxSwimDistance){
            System.out.println(type + " " + name + " справляется с заплывом");
        }else{
            System.out.println(type + " " + name + " не справляется с заплывом");
        }

    }



}
