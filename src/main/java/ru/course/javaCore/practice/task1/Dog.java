package ru.course.javaCore.practice.task1;

public class Dog extends Animal{
    public static int count;
    public Dog(String name, int maxRunDistance, int maxSwimDistance) {
        super("Собака", name, maxRunDistance, maxSwimDistance);
        count++;
    }

}
