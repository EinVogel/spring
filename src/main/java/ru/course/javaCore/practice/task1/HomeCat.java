package ru.course.javaCore.practice.task1;

public class HomeCat extends Cat{
    public static int count;
    public HomeCat(String name, int maxRunDistance) {
        super("Домашний кот", name, maxRunDistance, 0);
        count++;
    }
}
