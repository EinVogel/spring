package ru.course.javaCore.practice.task1;

public class Cat extends Animal{
    public static int count;

    public Cat(String type, String name, int maxRunDistance, int maxSwimDistance) {
        super("Кот", name, maxRunDistance, maxSwimDistance);
        count++;
    }
}
