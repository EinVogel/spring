package ru.course.javaCore.practice.task1;

public class Test {
    public static void main(String[] args) {
        Animal[] animals = {
                new HomeCat("Barsik", 200),
                new Dog("Бобик", 1000, 50),
                new Tiger("Тигра", 10000, 500)};
        for (Animal animal: animals){
            animal.run(800);
            animal.swim(50);
        }
        System.out.println("Котов " + Cat.count + " шт");
    }

}
