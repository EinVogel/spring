package ru.course.javaCore.practice.task3;

public class MyArraySizeException extends RuntimeException{

    public MyArraySizeException(String message) {
        super(message);
    }
}
