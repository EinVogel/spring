package ru.course.javaCore.practice.task3;

public class MyArrayDataException extends RuntimeException{

    public MyArrayDataException(int row_index, int column_index) {
        super("В элементах массива " + row_index + " " + column_index + " неправильный тип для преобразования в" +
                "целочисленный тип");
    }
}
