package ru.course.javaCore.practice.task3;

public class MainApp {
    public static void main(String[] args) {
        String[][] array = {
                {"1", "2", "3", "4"},
                {"1", "2", "3", "4"},
                {"1", "2", "3", "4"},
                {"1", "2", "3", "1"}};

        arraySum(array);
    }

    static void arraySum(String[][] array){
        int arraySum = 0;
        for (int row_index = 0; row_index < array.length; row_index++){
            if (array[row_index].length != array.length | array[row_index].length != 4) {
                throw new MyArraySizeException("Размер массива не 4х4");
            }
            for (int column_index = 0; column_index < array[row_index].length; column_index++) {
                try {
                    arraySum += Integer.parseInt(array[row_index][column_index]);
                }catch (NumberFormatException e){
                    throw new MyArrayDataException(row_index, column_index);
                }
            }
        }
        System.out.println("Сумма массива: " + arraySum);;
    }
}
