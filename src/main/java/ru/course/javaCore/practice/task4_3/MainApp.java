package ru.course.javaCore.practice.task4_3;

public class MainApp {
    public static void main(String[] args) {
        Box<Orange> boxOrange = new Box<>();
        Box<Apple> boxApple = new Box<>();
        Box<Apple> boxApple1 = new Box<>();

        for (int i = 0; i < 10; i++) {
            boxOrange.addFruit(new Orange(1.5f));
            boxApple.addFruit(new Apple(1.0f));
        }

        float boxAppleWeight = boxApple.getWeight();
        float boxOrangeWeight = boxOrange.getWeight();

        System.out.println("Вес коробки с яблоками: " + boxAppleWeight);
        System.out.println("Вес коробки с апельсинами: " + boxOrangeWeight);

        boxApple.compareTo(boxApple);

        boxApple.shiftFruitTo(boxApple1, 5);
        System.out.println(boxApple.fruits.size());
        System.out.println(boxApple1.fruits.size());

    }
}
