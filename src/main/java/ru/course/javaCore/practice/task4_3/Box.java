package ru.course.javaCore.practice.task4_3;

import java.util.ArrayList;
import java.util.List;

public class Box<T extends Fruit> {
    float weight;
    List<T> fruits = new ArrayList<>();

    void addFruit(T fruit){
        fruits.add(fruit);
    }

    float getWeight(){
        weight = 0;
        for (T fruit: fruits) {
            weight += fruit.weight;
        }
        return weight;
    }

    void compareTo(Box<? extends Fruit> comparableBox){
        if (Math.abs(getWeight() - comparableBox.getWeight()) < 0.0001){
            System.out.println("Коробки одинаковы по весу");
        }else {
            System.out.println("Коробки разные по весу");
        }
    }

    void shiftFruitTo(Box<T> box, int quantityFruit){
        int currentBoxSize = fruits.size();
        if (quantityFruit > currentBoxSize){
            System.out.println("В ящике " + currentBoxSize + " фруктов, нельзя взять оттуда " + quantityFruit + " шт");
        }else {
            for (int i = currentBoxSize - 1; i > currentBoxSize - quantityFruit - 1; i--) {
                T transferFruit = fruits.get(i);
                fruits.remove(transferFruit);
                box.addFruit(transferFruit);
            }
        }
    }
}
