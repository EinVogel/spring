package ru.course.javaCore.practice.task2;

public interface Obstacle {

    boolean canOvercoming();

    String overcoming(int attemptValue);
}
