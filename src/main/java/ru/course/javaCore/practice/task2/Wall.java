package ru.course.javaCore.practice.task2;

public class Wall implements Obstacle{

    int high;
    boolean overcoming;

    public Wall(int high){
        this.high = high;
    }

    @Override
    public boolean canOvercoming() {
        return overcoming;
    }

    @Override
    public String overcoming(int jumpHigh) {
        if (jumpHigh >= high){
            overcoming = true;
            return "Стена была преодолена";
        } else {
            overcoming = false;
            return "Стена не была преодолена";
        }
    }
}
