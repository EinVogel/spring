package ru.course.javaCore.practice.task2;

public interface AbleToJump {

    void jump(Obstacle obstacle);
}
