package ru.course.javaCore.practice.task2;

public class Racetrack implements Obstacle{
    int distance;
    boolean overcoming;

    public Racetrack(int length){
        this.distance = length;
    }

    @Override
    public boolean canOvercoming() {
        return overcoming;
    }

    @Override
    public String overcoming(int runDistance) {
        if (runDistance >= distance) {
            overcoming = true;
            return "Дорожка была преодолена";
        }
        else {
            overcoming = false;
            return "Дорожка не была преодолена";
        }
    }
}
