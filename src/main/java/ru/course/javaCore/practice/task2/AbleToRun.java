package ru.course.javaCore.practice.task2;

public interface AbleToRun {

    void run(Obstacle obstacle);
}
