package ru.course.javaCore.practice.task2;

public class Participant implements AbleToJump, AbleToRun{
    int maxRunDistance;
    int maxJumpHigh;
    String name;

    public Participant(String name, int maxRunDistance, int maxJumpHigh){
        this.name = name;
        this.maxRunDistance = maxRunDistance;
        this.maxJumpHigh = maxJumpHigh;
    }

    @Override
    public void jump(Obstacle obstacle) {
        System.out.println(name + ": " + obstacle.overcoming(maxJumpHigh));
    }

    @Override
    public void run(Obstacle obstacle) {
        System.out.println(name + ": " + obstacle.overcoming(maxRunDistance));
    }
}
