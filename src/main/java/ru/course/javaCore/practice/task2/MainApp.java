package ru.course.javaCore.practice.task2;

import java.util.ArrayList;
import java.util.List;

public class MainApp {
    public static void main(String[] args) {
        Participant human = new Participant("Mike", 2000, 150);
        Participant robot = new Participant("Skynet", 1000, 70);
        Participant cat = new Participant("Tom", 50, 70);

        Obstacle wall = new Wall(100);
        Obstacle racetrack = new Racetrack(1000);

        Participant [] participants = {human, robot, cat};
        List<Participant> activeParticipants = new ArrayList<>();

        for (Participant participant: participants){
            participant.jump(wall);
            if (!wall.canOvercoming()){
                continue;
            }
            participant.run(racetrack);
            if (!racetrack.canOvercoming()){
                continue;
            }
            activeParticipants.add(participant);

        }

        for (Participant participant: activeParticipants)
            System.out.println("Оставшиеся участники - " + participant.name);


        }
}
