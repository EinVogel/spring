package ru.course.javaCore;

import ru.course.javaCore.UpperDownCasting.Animal;

public class AnonymousClass {

    public static void main(String[] args) {
        Animal animal = new Animal();

        // Анониманые классы создаются тогда, когда объект будет использоваться один раз, поэтому нет смысла для этого
        // объекта отдельный класс, и переопределять его методы
        Animal otherAnimal = new Animal(){
            public void eat(){
                System.out.println("Other aninal eating");
            }
        };

        animal.eat();
        otherAnimal.eat();
    }
}
