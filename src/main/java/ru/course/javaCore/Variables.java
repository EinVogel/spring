package ru.course.javaCore;

public class Variables {
    // boolean: хранит значение true или false
    boolean isActive = false;
    boolean isAlive = true;

    // byte: хранит целое число от -128 до 127 и занимает 1 байт
    byte a_byte = 3;
    byte b_byte = 8;

    // short: хранит целое число от -32768 до 32767 и занимает 2 байта
    short a_short = 3;
    short b_short = 8;

    //int: хранит целое число от -2147483648 до 2147483647 и занимает 4 байта
    int a_int = 4;
    int b_int = 9;

    // long: хранит целое число от –9 223 372 036 854 775 808 до 9 223 372 036 854 775 807 и занимает 8 байт
    long a_long = 5L;
    long b_long = 10L;

    // double: хранит число с плавающей точкой от ±4.9*10-324 до ±1.8*10308 и занимает 8 байт
    double x_double = 8.5;
    double y_double = 2.7;

    //float: хранит число с плавающей точкой от -3.4*1038 до 3.4*1038 и занимает 4 байта
    float x_float = 8.5f;
    float y_float = 2.7f;

    // char: хранит одиночный символ в кодировке UTF-16 и занимает 2 байта,
    // поэтому диапазон хранимых значений от 0 до 65535
    char ch = 'a';

    String hello = "Hello...";
}
