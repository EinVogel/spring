package ru.course.javaCore.Comparation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
Конвенция по выводу результата сравнения между объектами
o > o1 ==> 1
o < o1 ==> -1
o == o1 ==> 0
 Компаратор используется для сортировки объектов между собой по своим правилам
 */

public class StringLengthComparator implements Comparator<String> {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("Mike");
        list.add("Tom");
        list.add("John");

        Collections.sort(list, new StringLengthComparator());
        System.out.println(list);
    }

    @Override
    public int compare(String o, String o1) {
        if (o.length() > o1.length()){
            return 1;
        }else if (o.length() < o1.length()) {
            return -1;
        }
        else{
            return 0;
        }
    }
}
