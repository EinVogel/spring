package ru.course.javaCore.Exceptions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.Scanner;

public class Test {
    // Checked Exception (Compile time exception) - искоючительные случаи в работе программы
    // Unchecked Exception (Runtime exception) - ошибки в работе программы
    public static void main(String[] args) {
        File file = new File("new_file.txt");
        try {
            Scanner scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.out.println("Не найден файл");
        }

        try {
            run();
        } catch (IOException  e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public static void run() throws IOException, ParseException {

    }

}
