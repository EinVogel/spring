package ru.course.javaCore;

import java.util.ArrayList;
import java.util.List;

public class Generics {
    public static void main(String[] args) {
        List<String> list = new ArrayList<String>();
        list.add("cat");
        System.out.println(list.toString());
    }
}
