package ru.course.javaCore.Multithreading;

public class MyThread extends Thread {
    // Ключевое слово volatile решает проблему коггерентности кэшей. Переменная running не будет записываться в кэш
    // ядра, а поместится в main memory
    // Если не устанавливать volatile, то в кэше одно ядра переменная volatile может быть true, а в кэше другого false,
    // Сравнение двух перемнных может с main memory может не произойти.
    // Когда один поток использует переменную, другой читает, используют volatile
    private volatile boolean running = true;

    public void run(){
        while (running){
            System.out.println("Hello");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void shutdown(){
        this.running = false;
    }
}
