package ru.course.javaCore.Multithreading;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

// Пример использования работы несколькими потоками над одной задачей
// Задача: открыть 3 замка параллельно 3 потоками
public class OpenLocks {
    public static void main(String[] args) throws InterruptedException {
        int locksCount = 3;
        int threadsCount = 3;
        CountDownLatch locks = new CountDownLatch(locksCount);

        ExecutorService executorService = Executors.newFixedThreadPool(threadsCount);

        for (int i = 0; i < locksCount; i++)
            executorService.submit(new Cracker(locks));

        // Мы назначили задания эти потокам, без него будет бесконечно работать
        executorService.shutdown();

        // Пока 3 замка не будут взломаны, поток будет ожидать здесь
        locks.await();
        System.out.println("Замки взломаны");
    }
}
