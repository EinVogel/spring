package ru.course.javaCore.Multithreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ThreadPool {
    public static void main(String[] args) throws InterruptedException {

        int workerCount = 2;
        ExecutorService executorService = Executors.newFixedThreadPool(workerCount);

        // Добавляем в список работ 10 коробок, которые должны отнести 2 рабочих
        for (int i = 1; i < 11; i++)
            executorService.submit(new Box(i));

        // Начинаем выполнение
        executorService.shutdown();
        System.out.println("Задачи поставлены");

        // Выставляем время, в течении которого мы будем ждать пока задачи будут выполнены
        executorService.awaitTermination(1, TimeUnit.HOURS);




    }
}
