package ru.course.javaCore.Multithreading;

import java.util.concurrent.Semaphore;

// semaphore ограничивает число потоков, которые получают доступ к ресурсу
public class flowLimiter {
    public static void main(String[] args) {
        int permittedThreads = 3;
        Semaphore semaphore = new Semaphore(permittedThreads);
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(semaphore.availablePermits());
        semaphore.release();
        System.out.println(semaphore.availablePermits());
    }


}
