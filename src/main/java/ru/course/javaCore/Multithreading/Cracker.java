package ru.course.javaCore.Multithreading;

import java.util.concurrent.CountDownLatch;

public class Cracker implements Runnable{
    private CountDownLatch locks;

    public Cracker(CountDownLatch locks){
        this.locks = locks;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        locks.countDown();
        System.out.println("Взлом одного замка");
    }
}
