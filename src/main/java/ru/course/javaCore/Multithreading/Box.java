package ru.course.javaCore.Multithreading;

public class Box implements Runnable {

    int id;

    public Box(int id){
        this.id = id;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Коробка " + id + " доставлена");
    }
}
