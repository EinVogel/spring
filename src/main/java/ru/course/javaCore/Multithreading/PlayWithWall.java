package ru.course.javaCore.Multithreading;

// Пример использования методов wait and notify
// Действие совершается над одним объектом, сначала вызываем метод hitBall, (бъем по мячу), ждем его возвращения
// с помощью метода wait(), который освобождает доступ к объекту play для других потоков, в методе takeBall
// мяч отскакивает от стены и летит обратно, через 2 секунды прилетает мяч, вызываем notify(), который запускает
// ожидающий поток
public class PlayWithWall {
    public static void main(String[] args) throws InterruptedException {

        final PlayWithWall play = new PlayWithWall();

        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    play.hitBall();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    play.takeBall();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();


    }

    private void hitBall() throws InterruptedException {
        synchronized (this){
            System.out.println("Ударил мячем по стене...");
            wait();
            System.out.println("Получил мяч, продолжил играть");
        }
    }

    private void takeBall() throws InterruptedException {
        Thread.sleep(2000);
        synchronized (this){
            notify();
            System.out.println("Мяч отскочил от стены");
        }
    }
}
