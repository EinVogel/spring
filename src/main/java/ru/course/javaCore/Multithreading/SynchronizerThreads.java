package ru.course.javaCore.Multithreading;

public class SynchronizerThreads {

    private int counter;

    public static void main(String[] args) {
        SynchronizerThreads synchronizerThreads = new SynchronizerThreads();
        synchronizerThreads.doWork();

    }

    // Только одному потому разрешается выполнять тело метода, не может такого случиться, чтобы 2 потока
    // инкрементировали переменную одновременно, если используем sy
    public synchronized void increment(){
        counter++;
    }

    public void doWork(){
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10000; i ++){
                    increment();
                }
            }
        });

        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10000; i ++){
                    increment();
                }
            }
        });

        thread1.start();
        thread2.start();

        // метод join ожидает окончания работы двух потоков
        try {
            thread1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(counter);
    }
}
