package ru.course.javaCore.Multithreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

// Ограничение до 10 потов из 100 обращения к сервису
public class ConnectionToServer {
    private static ConnectionToServer connectionToServer = new ConnectionToServer();
    private int connectionToServerCount;

    private ConnectionToServer(){

    }

    public Semaphore semaphore = new Semaphore(10);

    public static ConnectionToServer getConnectionToServer(){
        return connectionToServer;
    }

    public void work() throws InterruptedException {
        semaphore.acquire();
        try {
            doWork();
        } finally {
            semaphore.release();
        }

    }


    private void doWork() throws InterruptedException{
        synchronized (this){
            connectionToServerCount ++;
        }
        System.out.println(connectionToServerCount);

        Thread.sleep(5000);

        synchronized (this){
            connectionToServerCount --;
        }
    }
}

class TestConnection {
    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(200);

        final ConnectionToServer connectionToServer = ConnectionToServer.getConnectionToServer();
        for (int i = 0; i < 200; i++) {
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        connectionToServer.work();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.HOURS);
    }
}