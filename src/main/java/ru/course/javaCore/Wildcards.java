package ru.course.javaCore;

import ru.course.javaCore.UpperDownCasting.Animal;
import ru.course.javaCore.UpperDownCasting.Dog;

import java.util.ArrayList;
import java.util.List;

public class Wildcards {
    public static void main(String[] args) {
        List<Animal> listOfAnimals = new ArrayList<Animal>();
        List<Dog> listOfDogs = new ArrayList<Dog>();

        printList(listOfAnimals);


    }
    /*
       Object - super
       Animal - extends, super
       Dog - extends
     */

    // ? extends Animal означает что в параметры метола может быть передат массив с элементами Animal или
    // элементами наследующихся от Animal (Animal, Dog)
    // ? super Animal означает что в параметры метола может быть передат массив с элементами от
    // которых Animal наследовался и сам Animal (Object, Animal)
    static void printList(List<? super Animal> list){
        System.out.println(list.toString());
    }
}
