package ru.course.javaCore;

import java.util.Scanner;

public class SwitchCase {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите возраст");
        int age = scanner.nextInt();
        switch (age){
            case 0:
                System.out.println(0);
                break;
            case 1:
                System.out.println(1);
                break;
            default:
                System.out.println("Не одно из предыдущих событий н произошло");
        }
    }
}
