package ru.course.javaCore.Collections;

import java.util.*;

/**
 * Время добавления 100000 раз элемента в index 0 class java.util.ArrayList: 12 мс
 * Время чтения 100000 элементов начиная с 0 индекса class java.util.ArrayList: 7 мс
 * Время обновления 100000 элементов начиная в index 0 class java.util.ArrayList: 10 мс
 * Время удаления 100000 элементов начиная с 0 индекса class java.util.ArrayList: 299 мс
 *
 * Время добавления 100000 раз элемента в index 0 class java.util.LinkedList: 7 мс
 * Время чтения 100000 элементов начиная с 0 индекса class java.util.LinkedList: 4833 мс
 * Время обновления 100000 элементов начиная в index 0 class java.util.LinkedList: 4 мс
 * Время удаления 100000 элементов начиная с 0 индекса class java.util.LinkedList: 1989 мс
 */
public class TimeCounter {

    List<Integer> list;
    String addingInfo;
    String readingInfo;
    String updatingInfo;
    String removingInfo;
    String classType;
    private final int items = 1_000_00;


    public TimeCounter(List<Integer> list){
        this.list = list;
        classType = String.valueOf(list.getClass());
        measureTimeAdding();
        measureTimeReading();
        measureTimeUpdating();
        measureTimeRemoving();
    }

    /** Измерение времени добавления элементов */
    public void measureTimeAdding(){
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < items; i++){
            list.add(i);
        }
        long stopTime = System.currentTimeMillis();
        addingInfo = String.format("Время добавления %s раз элемента в index 0 %s: %s мс",
                items, classType, stopTime - startTime);
        System.out.println(addingInfo);
    }

    /** Измерение времени чтения элементов */
    public void measureTimeReading(){
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < list.size(); i++){
            list.get(i);
        }
        long stopTime = System.currentTimeMillis();
        readingInfo = String.format("Время чтения %s элементов начиная с 0 индекса %s: %s мс",
                items, classType, stopTime - startTime);
        System.out.println(readingInfo);
    }

    /** Измерение времени чтения элементов */
    public void measureTimeUpdating(){
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < list.size(); i++){
            list.set(0, i);
        }
        long stopTime = System.currentTimeMillis();
        updatingInfo = String.format("Время обновления %s элементов начиная в index 0 %s: %s мс",
                items, classType, stopTime - startTime);
        System.out.println(updatingInfo);
    }

    /** Измерение времени удаления элементов */
    public void measureTimeRemoving(){
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < list.size(); i++){
            list.remove(i);
        }
        long stopTime = System.currentTimeMillis();
        removingInfo = String.format("Время удаления %s элементов начиная с 0 индекса %s: %s мс\n",
                items, classType, stopTime - startTime);
        System.out.println(removingInfo);
    }

}
