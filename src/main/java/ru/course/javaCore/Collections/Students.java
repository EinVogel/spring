package ru.course.javaCore.Collections;

import java.util.*;

public class Students {

    public static void main(String[] args) {
        Map<Integer, String> studentsHashMap = new HashMap<>(); // объекты лежат беспорядочно
        Map<Integer, String> studentsLinkedMap = new LinkedHashMap<>(); // объекты лежат в той последовательности,
        // которой положили
        Map<Integer, String> studentsTreeMap = new TreeMap<>(); // объекты лежат сортированными по ключу

        studentsHashMap.put(1, "John");
        studentsHashMap.put(2, "Mike");

        for (Map.Entry<Integer, String> entry : studentsHashMap.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
            System.out.println(entry);
        }

        // Множества
        Set<String> studentsHashSet = new HashSet<>();
        Set<String> studentsLinkedSet = new LinkedHashSet<>();
        Set<String> studentsTreeSet = new TreeSet<>();

        studentsHashSet.add("Tom");
        studentsHashSet.add("Mike");
        studentsHashSet.add("John");

        studentsLinkedSet.add("James");
        studentsLinkedSet.add("Tom");

        //union
        studentsHashSet.addAll(studentsLinkedSet);
        System.out.println("Union: " + studentsHashSet);
        //intersection
        studentsHashSet.retainAll(studentsLinkedSet);
        System.out.println("Intersection" + studentsHashSet);
        //difference
        studentsHashSet.removeAll(studentsLinkedSet);
        System.out.println("Difference" + studentsHashSet);
    }
}
