package ru.course.javaCore.Collections;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        List<Integer> arrayList = new ArrayList<>();
        List<Integer> linkedList = new LinkedList<>();

        new TimeCounter(arrayList);
        new TimeCounter(linkedList);

    }
}
