package ru.course.javaCore.Collections.Queue;

public class Person {
    int id;

    public Person(int id){
        this.id = id;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                '}';
    }
}
