package ru.course.javaCore.AbstractClass;

// Абстрактный класс нужен тогда, когда некоторые методы будут одинаковы у наследников, а некоторые будет абстратные,
// Например метод eat будет одинаковый, поэтому у него будет тело, а метод maleSound разный
public abstract class Animal {

    public void eat(){
        System.out.println("Animal is eating");
    }

    public abstract void makeSound();
}
