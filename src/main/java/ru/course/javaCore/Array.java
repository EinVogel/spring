package ru.course.javaCore;

import java.util.Arrays;

public class Array {
    public static void main(String[] args){
        int[] numbers = new int[5];
        for (int i = 0; i<numbers.length; i++){
            numbers[i] = i + 10;
        }
        System.out.println(Arrays.toString(numbers));

        int[] numbers_2 = {1, 2, 3};
        System.out.println(Arrays.toString(numbers_2));
    }
}
