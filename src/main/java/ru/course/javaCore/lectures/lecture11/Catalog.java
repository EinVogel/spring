package ru.course.javaCore.lectures.lecture11;

import javax.persistence.*;

@Entity
@Table(name="catalogs")
public class Catalog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "title")
    private String title;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Catalog() {
    }

    public Catalog(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Catalog: " + id + " " + title;
    }
}
