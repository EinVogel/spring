package ru.course.javaCore.lectures.lecture11;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.hibernate.cfg.Configuration;

public class MainApp {
    public static void main(String[] args) {
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Catalog.class)
                .buildSessionFactory();

        Session session = null;
        readQuery(session, factory);

    }

    public static void createQuery(Session session, SessionFactory factory){
        try {
            // Create
            session = factory.getCurrentSession();
            Catalog catalog = new Catalog("Fantasy");
            session.beginTransaction();
            session.save(catalog);
            session.getTransaction().commit();


        } finally {
            factory.close();
            session.close();
        }
    }

    public static void readQuery(Session session, SessionFactory factory){
        try {
            session = factory.getCurrentSession();
            session.beginTransaction();
            // Получаем запись с id
            Catalog catalog = session.get(Catalog.class, 1L);
            session.getTransaction().commit();
            System.out.println("catalog " + catalog);

        } finally {
            factory.close();
            session.close();
        }
    }

    public static void setQuery(Session session, SessionFactory factory){
        try {
            session = factory.getCurrentSession();
            session.beginTransaction();
            Catalog catalog = session.get(Catalog.class, 1L);
            catalog.setTitle("fan");
            session.getTransaction().commit();
            System.out.println("catalog " + catalog);

        } finally {
            factory.close();
            session.close();
        }
    }

    public static void deleteQuery(Session session, SessionFactory factory){
        try {
            session = factory.getCurrentSession();
            session.beginTransaction();
            Catalog catalog = session.get(Catalog.class, 1L);
            session.delete(catalog);
            session.getTransaction().commit();
            System.out.println("catalog " + catalog);

        } finally {
            factory.close();
            session.close();
        }
    }




}
