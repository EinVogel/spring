package ru.course.javaCore.lectures.lecture8.example1;

public class mainApp {
    public static void main(String[] args) {
        doSomething(() -> {
            for (int i = 0; i < 5; i++) {
                System.out.println("java");
            }
        });

        doSomething(() -> {
            System.out.println("Language Java");
        });

    }

    static void doSomething(Runnable object){
    }
}