package ru.course.javaCore.lectures.lecture4.example1;

public class Genbox<T> {

    T obj;

    public T getObj() {
        return obj;
    }

    public void setObj(T obj) {
        this.obj = obj;
    }

    public Genbox(T obj) {
        this.obj = obj;
    }
}
