package ru.course.javaCore.lectures.lecture2.example4;

import ru.course.javaCore.UpperDownCasting.Animal;
import ru.course.javaCore.lectures.lecture2.example1.Flyable;

// Внешний объект может жить без внутреннего, а внутренний без внегнего нет
public class MainApp {
    class Inner {
        int innerValue;

        void innerMethod(){
            // Внутренний класс видит внешние переменные и классы
            System.out.println(outerValue);
            outerMethod();

        }
    }

    int outerValue;

    void outerMethod(){
        // Но внешний класс не видит методы и поля внутреннего
        // System.out.println(innerValue);

    }

    // Статический класс может жить без внешнего
    static class StaticInner{

    }

    public static void main(String[] args) {
        StaticInner staticInner = new StaticInner();

        // Можно создать класс из из Абстрактного класса и Интерфейса, только класс будет анонимный
        Flyable flyable = new Flyable(){

            @Override
            public void fly() {

            }
        };

        ChessFigure chessFigure = new ChessFigure() {
            @Override
            void doStep() {

            }
        };

        System.out.println(flyable.getClass().getName());
        System.out.println(chessFigure.getClass().getName());
    }
}
