package ru.course.javaCore.lectures.lecture2.example2;

public class Car implements Transport{
    @Override
    public void start() {
        System.out.println("Человек поехал на машине");
    }

    @Override
    public void stop() {
        System.out.println("Человек остановил машину");
    }
}
