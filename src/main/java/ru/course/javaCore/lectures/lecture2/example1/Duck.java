package ru.course.javaCore.lectures.lecture2.example1;

public class Duck implements Flyable, Swimmable {
    @Override
    public void fly() {
        System.out.println("Утка летит");
    }

    @Override
    public void swim() {

    }
}
