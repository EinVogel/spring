package ru.course.javaCore.lectures.lecture2.example2;

public class Moto implements Transport{
    @Override
    public void start() {
        System.out.println("Человек поехал на мотоцикле");
    }

    @Override
    public void stop() {
        System.out.println("Человек остановил мотоцикл");
    }

    void climbTheRearWheel(){
        System.out.println("Человек поднял мотоцикл на заднее колесо");
    }

}
