package ru.course.javaCore.lectures.lecture2.example4;

public abstract class ChessFigure {

    abstract void doStep();
}
