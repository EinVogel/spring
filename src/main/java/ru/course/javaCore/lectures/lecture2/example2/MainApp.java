package ru.course.javaCore.lectures.lecture2.example2;

public class MainApp {
    public static void main(String[] args) {
        Human human = new Human();
        Transport car = new Car();
        human.drive();
        human.stop();
        human.drive(car);
        human.stop();
        human.makeTrackAction();
    }
}
