package ru.course.javaCore.lectures.lecture2.example1;

public interface Swimmable {
    void swim();
}
