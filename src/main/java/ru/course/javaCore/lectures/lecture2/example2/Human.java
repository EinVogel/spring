package ru.course.javaCore.lectures.lecture2.example2;

public class Human {
    Transport currentTransport;

    void drive(Transport transport){
        currentTransport = transport;
        currentTransport.start();
    }

    void drive(){
        if (currentTransport == null)
            System.out.println("У меня нет транспорта");
        else {
            currentTransport.start();
        }
    }

    void stop(){
        if (currentTransport == null)
            System.out.println("Я никуда и не еду");
        else {
            currentTransport.stop();
        }
    }

    void makeTrackAction(){
        if (!(currentTransport instanceof Moto)){
            System.out.println("Я сейчас не на мотоцикле");
        }else{
            ((Moto) currentTransport).climbTheRearWheel();
        }
    }
}
