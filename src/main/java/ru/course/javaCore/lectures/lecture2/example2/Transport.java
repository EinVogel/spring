package ru.course.javaCore.lectures.lecture2.example2;

public interface Transport {
    void start();

    void stop();
}
