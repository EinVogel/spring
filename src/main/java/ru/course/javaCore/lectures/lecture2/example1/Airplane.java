package ru.course.javaCore.lectures.lecture2.example1;

public class Airplane implements Flyable {
    @Override
    public void fly() {
        System.out.println("Самолет летит");
    }
}
