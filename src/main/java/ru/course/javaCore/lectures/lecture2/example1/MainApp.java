package ru.course.javaCore.lectures.lecture2.example1;

import ru.course.javaCore.lectures.lecture2.example1.Airplane;
import ru.course.javaCore.lectures.lecture2.example1.Duck;
import ru.course.javaCore.lectures.lecture2.example1.Flyable;

public class MainApp {
    public static void main(String[] args) {
        Flyable[] flyables = {
                new Duck(),
                new Airplane()};

    for (Flyable item: flyables){
        item.fly();

    }
    }
}
