package ru.course.javaCore.lectures.lecture9;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MainApp {
    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {
        Class catClass = Cat.class;
        Method[] methods = catClass.getMethods();
        // Чтобы посмотреть публичные методы
        for (Method method: methods) {
            System.out.println(method.getName());
        }

        Method[] declaredMethods = catClass.getDeclaredMethods();
        // Чтобы посмотреть методы внутри объявленного класса
        for (Method method: declaredMethods) {
            System.out.println(method.getName());
        }

        Cat cat = new Cat(1, 2, 3);
        // Вызов метода из reflection API
        methods[0].invoke(cat);
        // Дали доступ из класса MainApp для приватного метода
        declaredMethods[1].setAccessible(true);
        declaredMethods[1].invoke(cat);
    }
}
