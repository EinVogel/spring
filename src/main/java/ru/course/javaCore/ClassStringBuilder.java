package ru.course.javaCore;

public class ClassStringBuilder {
    public static void main(String[] args){
        // Класс String - неизменяеймый объект, при конкатенации строк создаются новые объекты. Класс StringBuilder
        // складывает строки в один объект
        StringBuilder stringBuilder = new StringBuilder("Hello");
        stringBuilder.append(" my").append(" friend");
        System.out.println(stringBuilder);
    }

}
