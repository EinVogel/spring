package ru.course.SpringCore.lections.lesson13.examples;

public class JavaCodeCreator implements CodeCreator{
    ClassNameGenerator classNameGenerator;

    public void setClassNameGenerator(ClassNameGenerator classNameGenerator){
        this.classNameGenerator = classNameGenerator;
    }
    @Override
    public String getClassName() {
        return "public class " + classNameGenerator.generateClassName() + " {\n\n}";
    }
}
