package ru.course.SpringCore.lections.lesson13.examples;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AppSpring {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        SimpleBean simpleBean = context.getBean("simpleBean", SimpleBean.class);
        simpleBean.doSomething();

        CodeCreator javaCodeCreator = context.getBean("javaCodeCreator", CodeCreator.class);
        System.out.println(javaCodeCreator.getClassName());

        AnnotatedBean annotatedBean = context.getBean("annotatedBean", AnnotatedBean.class);
        annotatedBean.example();


    }
}
