package ru.course.SpringCore.lections.lesson13.simpleApp;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.course.SpringCore.lections.lesson13.examples.AppConfig;
import ru.course.SpringCore.lections.lesson13.examples.SimpleBean;

public class App {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        ShopService shopService = context.getBean("shopService", ShopService.class);
        shopService.buy();
        context.close();
    }
}
