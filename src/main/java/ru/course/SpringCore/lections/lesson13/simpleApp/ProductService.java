package ru.course.SpringCore.lections.lesson13.simpleApp;

import org.springframework.stereotype.Component;

@Component
public class ProductService {
    public void getProducts(){
        System.out.println("Получен список товаров");
    }
}
