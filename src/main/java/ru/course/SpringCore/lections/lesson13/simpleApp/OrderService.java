package ru.course.SpringCore.lections.lesson13.simpleApp;

import org.springframework.stereotype.Component;

@Component
public class OrderService {
    public void createOrder(){
        System.out.println("Заказ сформирован");
    }
}
