package ru.course.SpringCore.lections.lesson13.simpleApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class ShopService {
    private MailService mailService;
    private ProductService productService;

    @Autowired
    @Qualifier("bean1")
    public void setBean(BeanInterface bean) {
        this.bean = bean;
    }

    private BeanInterface bean;

    @Autowired
    public ShopService(MailService mailService) {
        this.mailService = mailService;
    }

    // dependency injection по сеттеру
    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }
    // dependency injection по полю
    @Autowired
    OrderService orderService;

    public void buy(){
        productService.getProducts();
        orderService.createOrder();
        mailService.sendMail();
    }
}
