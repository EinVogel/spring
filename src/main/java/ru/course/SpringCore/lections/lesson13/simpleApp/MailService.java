package ru.course.SpringCore.lections.lesson13.simpleApp;

import org.springframework.stereotype.Component;

@Component
public class MailService {
    public void sendMail(){
        System.out.println("Письмо отправлено");
    }
}
