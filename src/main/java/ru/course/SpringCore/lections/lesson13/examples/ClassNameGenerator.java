package ru.course.SpringCore.lections.lesson13.examples;

public interface ClassNameGenerator {
    String generateClassName();

}
