package ru.course.SpringCore.lections.lesson13.examples;

public class AnimalsClassNameGenerator implements ClassNameGenerator{
    @Override
    public String generateClassName() {
        String[] names = {"Cat", "Dog", "Horse", "Bull"};
        return names[(int) (Math.random() * 4)];
    }
}
