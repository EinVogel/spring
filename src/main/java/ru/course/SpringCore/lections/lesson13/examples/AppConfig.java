package ru.course.SpringCore.lections.lesson13.examples;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.course.SpringCore.lections.lesson13")
public class AppConfig {

    @Bean(name = "javaCodeCreator")
    public CodeCreator javaCodeCreator(){
        JavaCodeCreator codeCreator = new JavaCodeCreator();
        codeCreator.setClassNameGenerator(animalClassNameGenerator());
        return codeCreator;
    }

    @Bean
    public ClassNameGenerator animalClassNameGenerator(){
        return new AnimalsClassNameGenerator();
    }

    @Bean
    public SimpleBean simpleBean(){
        return new SimpleBean();
    }
}
