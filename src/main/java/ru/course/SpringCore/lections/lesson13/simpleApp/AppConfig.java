package ru.course.SpringCore.lections.lesson13.simpleApp;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.course.SpringCore.lections.lesson13.simpleApp")
public class AppConfig {

}